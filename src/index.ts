const Application = ({ file }: { file: String }) => {
    const childProcess = require('child_process');
    const electron = require('electron');
    const path = require('path');

    // electron lib/electron.js --enable-transparent-visuals --disable-gpu
    // childProcess.spawn(electron, [path.join(__dirname, file)], {
    childProcess.spawn(electron, [
        path.join(__dirname, 'lib/electron.js'),
        file
    ], {
        stdio: 'inherit'
    })
    .on('close', function () {
        process.exit();
    });
}


export default Application;
