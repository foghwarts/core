/// <reference path="../../typings/index.d.ts" />

import * as program from "commander";
import * as path from "path";
import Application from "../index";

const dashboard = require('../../package');

interface InterfaceCLI extends commander.ICommand {
  config?: string
}

export namespace Dashboard {
  program
    .version(dashboard.version)
    .option('-c, --config [file]', 'Specifies config / HTML file to load')

  const cli: InterfaceCLI = program.parse(process.argv);
  if (!cli.config) {
    cli.config = path.join(__dirname, '../../example/tutorial.html');
  } else {
    cli.config = (path.isAbsolute(cli.config)) ? cli.config : path.join(process.cwd(), cli.config);
  }

  Application({
    file: cli.config
  });
}
