export namespace ElectronLauncher {
  const electron = require('electron')
  const app = electron.app
  const BrowserWindow = electron.BrowserWindow
  require('electron-reload')(__dirname, {
    electron: require('electron-prebuilt')
  });

  let mainWindow : Electron.BrowserWindow;

  function createWindow () {
    mainWindow = new BrowserWindow({
      width: 800,
      height: 600,
      x: 1200,
      y:100,

      type: 'desktop',  // desktop, dock, toolbar, splash, notification
      frame: false,
      transparent: true,
      // titleBarStyle: 'hidden',
      // fullscreen: true,
      title: 'Dashboard',
      // useContentSize: true,
      // alwaysOnTop: true,
    });

    // mainWindow.loadURL(`file://${__dirname}/index.html`)
    mainWindow.loadURL(`file://${process.argv[2]}`);

    mainWindow.webContents.openDevTools()

    mainWindow.on('closed', function () {
      mainWindow = null
    })
  }

  app.on('ready', createWindow)

  app.on('window-all-closed', function () {
    if (process.platform !== 'darwin') {
      app.quit()
    }
  })

  app.on('activate', function () {
    if (mainWindow === null) {
      createWindow()
    }
  })

}
