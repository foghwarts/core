# ![Dashboard logo](http://findicons.com/files/icons/1261/sticker_system/48/dashboard.png) Dashboard

### Installation

Clone the repository
```sh
$ git clone git clone git@bitbucket.org:foghwarts/core.git
```

Install dependencies
```sh
$ npm i
```

Run
```sh
$ npm start
```

WIP
